package com.example.temperaturecalculator;

public class Calculator {

    public double conversion(String temperature){
        double u = Double.parseDouble(temperature);
        return (u*1.8 +32);
    }
}
